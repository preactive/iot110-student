[LAB2 INDEX](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab2/setup.md)

### Part E - Javascript Click Events ###
**Objective:** Now that we have almost everyone working, we need to add some
relatively simple jQuery to intercept button click events and then call the
GPIO methods by way of HTTP API methods.  In the ```<script>``` section, add the
following design pattern for each of the LEDs.  Even if you don't know much or
any jQuery you can sort of logically determine that an HTML element given by
the particular ID corresponding to the LED color will fire a button event which
will get processed by the jQuery JS click event.

```html
  <script>
  // start executing only after document has loaded
  $(document).ready(function() {
    // establish global variables for LED status
    var led1;
    var led2;
    var led3;

    // The button click functions run asynchronously in the browser
    $('#red_led_btn').click(function() {
      if (led1 === "OFF") {led1 = "ON";} else {led1 = "OFF";}
      var params = 'led=1&state='+led1;
      console.log('Led Command with params:' + params);
      $.post('/ledcmd', params, function(data, status){
              console.log("Data: " + data + "\nStatus: " + status);
      });
    });

    // code the green and blue buttons the same
  });
  </script>
```
Complete the same design pattern for each of the buttons but changing the POST
API data parameter to correspond to the correct LED.  Note that we have code for
toggling the led1 variable each time the click occurs.

[LAB2 INDEX](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab2/setup.md)
